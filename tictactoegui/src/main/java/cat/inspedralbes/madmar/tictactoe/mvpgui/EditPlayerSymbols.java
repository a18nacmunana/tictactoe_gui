package cat.inspedralbes.madmar.tictactoe.mvpgui;

import javax.swing.JPanel;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.FlowLayout;

public class EditPlayerSymbols extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField symbol1,symbol2;
	private JLabel name1,name2;

	/**
	 * Create the panel.
	 */
	public EditPlayerSymbols() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
						
		JLabel lblPlayer1 = new JLabel("Player");
		add(lblPlayer1);
		
		name1 = new JLabel("1");
		add(name1);
		
		symbol1 = new JTextField();
		symbol1.setPreferredSize(new Dimension(50,20));
		add(symbol1);
		
		JLabel lblPlayer2 = new JLabel("Player");
		add(lblPlayer2);
		
		name2 = new JLabel("2");
		add(name2);
		
		symbol2 = new JTextField();
		symbol2.setPreferredSize(new Dimension(50,20));
		add(symbol2);
		
		double size1= (lblPlayer1.getPreferredSize().getWidth()+name1.getPreferredSize().getWidth()+symbol1.getPreferredSize().getWidth());
		double size2= (lblPlayer1.getPreferredSize().getWidth()+name2.getPreferredSize().getWidth()+symbol2.getPreferredSize().getWidth());
		double max=size1+30;
		if (size2>size1)
			max=size2+30;
		setPreferredSize(new Dimension((int)max,60));
	}
	
	

	public String getPlayer1symbol() {
		return symbol1.getText();
	}
	public void setPlayer1symbol(String text) {
		symbol1.setText(text);
	}
	public String getPlayer2symbol() {
		return symbol2.getText();
	}
	public void setPlayer2symbol(String text) {
		symbol2.setText(text);
	}
	public String getPlayer1name() {
		return name1.getText();
	}
	public void setPlayer1name(String text) {
		name1.setText(text);
	}
	public String getPlayer2name() {
		return name2.getText();
	}
	public void setPlayer2name(String text) {
		name2.setText(text);
	}
}
