package cat.inspedralbes.madmar.tictactoe.mvpgui;

import cat.inspedralbes.madmar.tictactoe.model.Jugada;

public interface IBoardView {
	
	public void clearBoard();	
	public void enableBoard(boolean enabled);
	public void clickTrickButton();
	public void setPlay(Jugada jugada,char symbol);
}
