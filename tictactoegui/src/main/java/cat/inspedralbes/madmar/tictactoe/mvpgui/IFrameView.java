package cat.inspedralbes.madmar.tictactoe.mvpgui;

public interface IFrameView {
	
	public void showWinner(String text);
	public void hideWinner();
	public void showTurn(boolean visibility);
	public void hideTurn();
	public void clearGUI();
	public void enableOptionChanges(boolean enabled);
	public void setPlayersSymbols(char s1,char s2);
	public boolean isIAModeSelected();
	public String[] getPlayersSymbol();
	public BoardPanel getBoard();

}
