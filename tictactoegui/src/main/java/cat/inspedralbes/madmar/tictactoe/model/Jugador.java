package cat.inspedralbes.madmar.tictactoe.model;

import java.awt.Color;

public abstract class Jugador {
	
	private char symbol;
	private String name;
	private Color color;
		
	public Jugador(String name,char symbol) {
		this.symbol=symbol;
		this.name=name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getSymbol() {
		return symbol;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public void setColor(Color color) {
		this.color=color;
	}
	protected abstract Jugada obtenirJugadaGui(String posicio);	
}
