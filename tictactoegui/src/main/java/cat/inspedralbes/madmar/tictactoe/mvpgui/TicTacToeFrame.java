package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class TicTacToeFrame extends JFrame implements ActionListener,IFrameView{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BoardPanel board;
	private PlayerInfoPanel[] playersInfo=new PlayerInfoPanel[2];
	private JLabel lblWinner;	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JMenu mnMode,mnView;
	private EditPlayerSymbols editSymbols;
	private EditPlayerColors editColors;
	private JRadioButtonMenuItem rdbtnmntmHumanVsAi;
	private MyPresenter presenter;
	
	/**
	 * Create the frame.
	 */
	public TicTacToeFrame() {
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Object[] options = {"Exit","Cancel"};
				int resp = JOptionPane.showOptionDialog(null, "Do you want to leave?", "Warning",
						JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
						null, options, options[0]);

			 	System.out.println(resp);
				if(resp==0) 
			 		dispose();
			}
		});
		setTitle("Tic Tac Toe");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnGame = new JMenu("Game");
		menuBar.add(mnGame);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(this);
		mnGame.add(mntmNew);
		
		mnMode = new JMenu("Mode");
		menuBar.add(mnMode);
		
		rdbtnmntmHumanVsAi = new JRadioButtonMenuItem("Human vs AI");
		buttonGroup.add(rdbtnmntmHumanVsAi);
		mnMode.add(rdbtnmntmHumanVsAi);
		rdbtnmntmHumanVsAi.setSelected(true);
		
		JRadioButtonMenuItem rdbtnmntmHumanVsHuman = new JRadioButtonMenuItem("Human vs Human");
		buttonGroup.add(rdbtnmntmHumanVsHuman);
		mnMode.add(rdbtnmntmHumanVsHuman);
		
		mnView = new JMenu("View");
		menuBar.add(mnView);
		
		JMenuItem mntmSymbols = new JMenuItem("Symbols");
		mntmSymbols.addActionListener(this);
		mnView.add(mntmSymbols);
		
		JMenuItem mntmColors = new JMenuItem("Colors");
		mntmColors.addActionListener(this);
		mnView.add(mntmColors);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(this);
		mnHelp.add(mntmAbout);
		
		JMenuItem mntmOnlineHelp = new JMenuItem("Online help");
		mntmOnlineHelp.setActionCommand("Online");
		mntmOnlineHelp.addActionListener(this);
		mnHelp.add(mntmOnlineHelp);

		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel top = new JPanel();
		contentPane.add(top, BorderLayout.NORTH);
		
		JPanel bottom = new JPanel();
		contentPane.add(bottom, BorderLayout.SOUTH);
		
		lblWinner = new JLabel("Guanyador: ");
		bottom.add(lblWinner);
		
		PlayerInfoPanel left = new PlayerInfoPanel();
		contentPane.add(left, BorderLayout.WEST);
				
		PlayerInfoPanel right = new PlayerInfoPanel();
		right.setPlayerName("2");
		right.setPlayerSymbol("O");
		contentPane.add(right, BorderLayout.EAST);
		
		JPanel center = new JPanel();
		contentPane.add(center, BorderLayout.CENTER);
		
		board = new BoardPanel();
		board.setPreferredSize(new Dimension(250, 250));
		center.add(board);
		
		editSymbols=new EditPlayerSymbols();
		playersInfo[0]=left;
		playersInfo[1]=right;
		
		clearGUI();
		
		pack();		
			
	}

	public void init(MyPresenter presenter)
	{
		this.presenter=presenter;
		board.init(presenter);
	}
	
	// Mètodes de modificació de la GUI iniciats a través d'esdeveniments
	// Obtenir informació de la GUI		
	// Cridar al presentador	
	@Override
	public void actionPerformed(ActionEvent arg0) {

		switch (arg0.getActionCommand()) {
		case "New":						
			onclickNewGame();
			break;
		case "Symbols":		
			onClickEditSymbols();				
			break;
		case "Colors":
			onClickEditColors();
			break;
		case "About":
			presenter.onClickAbout();
			break;
		case "Online":
			presenter.onClickOnline();
			break;
		default:			
		}		
	}
	
	private void onclickNewGame() {
		presenter.onClickNewGame();
	}
	private void onClickEditColors() {

	}
		
	private void onClickEditSymbols()
	{
		Object[] options = {"Edit","Cancel"};		
		
		editSymbols.setPlayer1symbol(playersInfo[0].getPlayerSymbol());
		editSymbols.setPlayer2symbol(playersInfo[1].getPlayerSymbol());
		
		if (JOptionPane.showOptionDialog(null,editSymbols,"Player symbols", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,options,options[0])==0)
		{
			presenter.onClickEditSymbols(editSymbols.getPlayer1symbol(),editSymbols.getPlayer2symbol());				
		}				
	}
	
	// Mètodes de modificació de la GUI definits a la interface i iniciats al Presentador	
	@Override
	public void clearGUI() {			
		enableOptionChanges(true);
		//revisar
		showTurn(true); 			
		hideWinner();
		board.clearBoard();
		
	}
		
	@Override
	public void hideTurn() {
		setTurnPlayer1(false);
		setTurnPlayer2(false);		
	}

	@Override
	public void showTurn(boolean visibility) {
		setTurnPlayer1(visibility);
		setTurnPlayer2(!visibility);
		
	}

	@Override
	public void showWinner(String text) {		
		setWinnerInfo(text);
		setWinnerInfoVisible(true);		
	}
	
	@Override
	public void hideWinner() {
		lblWinner.setVisible(false);		
	}
	
	@Override
	public void enableOptionChanges(boolean enabled) {
		enableMode(enabled);
		enableView(enabled);
	}
	
	@Override
	public void setPlayersSymbols(char s1,char s2) {
		playersInfo[0].setPlayerSymbol(s1+"");
		playersInfo[1].setPlayerSymbol(s2+"");
	}

	@Override
	public String[] getPlayersSymbol() {		
		String[] symbols=new String[2];
		symbols[0]=playersInfo[0].getPlayerSymbol();
		symbols[1]=playersInfo[1].getPlayerSymbol();		
		return symbols;
	}

	@Override
	public boolean isIAModeSelected() {				
		return rdbtnmntmHumanVsAi.isSelected();
	}
	
	public boolean isWinnerInfoVisible() {
		return lblWinner.isVisible();
	}

	public BoardPanel getBoard() {
		return board;
	}
	
	public String getWinnerInfo() {
		return lblWinner.getText();
	}
	
	// Altres mètodes
	private void setWinnerInfo(String text) {
		lblWinner.setText(text);
	}

	private void setWinnerInfoVisible(boolean visible) {
		lblWinner.setVisible(visible);
	}
	
	private void setTurnPlayer1(boolean visibility) {
		playersInfo[0].setLblYourTurnVisible(visibility);
	}
	
	private void setTurnPlayer2(boolean visibility) {
		playersInfo[1].setLblYourTurnVisible(visibility);
	}	

	private void enableMode(boolean enabled) {
		mnMode.setEnabled(enabled);
	}

	private void enableView(boolean enabled) {		
		mnView.setEnabled(enabled);
	}
	public static JToolBar getToolBar() {
		JToolBar toolbar = new JToolBar();
		
		JButton btn1 = new JButton("btn1");
		btn1.setToolTipText("Aquest es el boto 1");
		toolbar.add(btn1);
		
		JButton btn2 = new JButton("btn2");
		btn2.setToolTipText("Aquest es el boto 2");
		toolbar.add(btn2);
		
		JButton btn3 = new JButton("btn3");
		btn3.setToolTipText("Aquest es el boto 3");
		toolbar.add(btn3);
		return toolbar;
	}

}
