package cat.inspedralbes.madmar.tictactoe.model;

import java.util.Random;

//Aquesta classe s'encarrega de gestionar l'ordre de les accions que transcorren
//en una partida. Controla els jugadors, el torn i si la partida ha acabat i qui guanya.
//
//La classe Joc no sap que internament Tauler es guarda la informacio en enters
//La classe Tauler no sap que Joc es guarda la informacio en objectes Jugador
public class Joc {

	private Tauler tauler;
	private boolean partidaEnJoc = true;
	private int tornJugador=0;					

	private Jugador[] jugadors=new Jugador[2];
	private Jugador guanyador = null;

	//Constructor
	public Joc(Tauler tauler, Jugador[] jugadors) {
		this.tauler = tauler;
		this.jugadors = jugadors;
		// Revisar -> El torn es decideix aleatòriament en construir el joc: tornJugador=(new Random()).nextInt(2);			
	}

	public Jugada ferJugadaGui(String posicio) {

		Jugada jugada = jugadors[tornJugador].obtenirJugadaGui(posicio);		
		
		tauler.posarFitxa(jugada.getFila(), jugada.getCol(), tornJugador+1);
		
		tornJugador=(tornJugador+1)%2;
		
		comprovarFinalPartida();
		
		return jugada;	

	}
	
	public boolean partidaEnJoc() {
		return partidaEnJoc;
	}
	
	//Es mira el tauler per veure si ja s'ha acabat
	private void comprovarFinalPartida() {
		if (tauler.esFinalPartida()) {
			partidaEnJoc = false;
		}
	}

	//Obtenim l'objecte jugador del guanyador o null si hi ha empat o el joc no ha acabat
	public Jugador obtenirGuanyador() {

		int g=tauler.obtenirGuanyador();
		
		if (g!=0)
			guanyador=jugadors[g-1];
		
		return guanyador;

	}
	
	public String obtenirSymbolGuanyador() {

		int g=tauler.obtenirGuanyador();
		
		String s = null;
		
		if (g!=0)
			s=jugadors[g-1].getSymbol()+"";
		else
			s="Tie";
		return s;

	}
	
	public char obtenirSymbolJugadorActual()
	{
		return jugadors[tornJugador].getSymbol();
	}

	//El toString del joc retorna el tauler mes la informacio de si el joc
	//encara sesta jugant aixi com si hi ha guanyador
	@Override
	public String toString() {
		String string = tauler.toString();
		
		string += "Estat: ";
		
		if (partidaEnJoc) {
			string += "en joc";
		} else {
			if (obtenirGuanyador() == jugadors[0]) {
				string += "guanya jugador 1";
			} else if (obtenirGuanyador() == jugadors[1]) {
				string += "guanya jugador 2";
			} else {
				string += "empat";
			}
		}
		
		string += "\n";
		return string;
	}

	public int getTornJugador() {
		return tornJugador;
	}
	
	public boolean isTornJugador1() {		 
		return (tornJugador==0);
	}
}
