package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import cat.inspedralbes.madmar.tictactoe.model.Jugada;

public class BoardPanel extends JPanel implements ActionListener, IBoardView{

	private MyPresenter presenter;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JToggleButton[][] buttons=new JToggleButton[3][3];
	private JToggleButton btnTrick;
	
	/**
	 * Create the panel.
	 */
	public BoardPanel() {
				
		setBorder(new EmptyBorder(10, 10, 10, 10));

        setPreferredSize(new Dimension(190, 190));
        setLayout(new GridLayout(3, 3, 1, 1));
        
        for (int i=0;i<3;i++)
        {
        	int posY=i;
        	for (int j=0;j<3;j++)
        	{
		    	int posX=j;
        		buttons[i][j] = new JToggleButton("");
        		buttons[i][j].addMouseListener(new java.awt.event.MouseAdapter() {
        		    public void mouseEntered(java.awt.event.MouseEvent evt) {
        		    	if(buttons[posY][posX].isEnabled())
        		    		buttons[posY][posX].setBackground(Color.GREEN);
        		    }
        		    public void mouseExited(java.awt.event.MouseEvent evt) {
        		    	buttons[posY][posX].setBackground(null);
        		    }
        		});
        		buttons[i][j].addActionListener(this);
        		buttons[i][j].setActionCommand(i+","+j);
                add(buttons[i][j]);
        	}
        }
        
		btnTrick = new JToggleButton("");
		btnTrick.setPreferredSize(new Dimension(1,1));
		btnTrick.setActionCommand(3+","+3);
		btnTrick.addActionListener(this);
	
	}

	public void init(MyPresenter presenter)
	{
		this.presenter=presenter;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		// Obtenir informació de la GUI
		// Cridar al presentador
		presenter.onClickBoardButton(arg0);
	
	}
	
	public void enableBoard(boolean enabled){
        for (int i=0;i<3;i++)
        {
        	for (int j=0;j<3;j++)
        	{
        		buttons[i][j].setEnabled(enabled);
        	}
        }
	}
	
	public void clearBoard(){
        for (int i=0;i<3;i++)
        {
        	for (int j=0;j<3;j++)
        	{
        		buttons[i][j].setEnabled(true);
        		buttons[i][j].setText("");
        		buttons[i][j].setSelected(false);
        	}
        }
	}

	@Override
	public void clickTrickButton() {		
		btnTrick.doClick(500);		
	}

	@Override
	// Modify !
	public void setPlay(Jugada jugada,char symbol) {	
		
		int fila=jugada.getFila();
		int col=jugada.getCol();
		
		buttons[fila][col].setText(symbol+"");
		buttons[fila][col].setSelected(true);
		buttons[fila][col].setEnabled(false);
	}
	
}