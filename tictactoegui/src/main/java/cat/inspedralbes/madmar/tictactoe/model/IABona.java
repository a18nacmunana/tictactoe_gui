package cat.inspedralbes.madmar.tictactoe.model;

//Aquesta IA no realitza jugades de manera optima 100% pero es millor que la random
public class IABona extends Jugador {

	private Tauler tauler;

	public IABona(String name,char symbol,Tauler tauler) {
		super(name,symbol);
		this.tauler = tauler;
	}

	@Override
	protected Jugada obtenirJugadaGui(String text) {
		
		// paràmetre no fet servir
		//String text
		
		// Es van mirant les seguents coses en ordre d'importancia. En el primer lloc on
		// es pugui fer la jugada es para
		//
		// 1. Jugar en una fila, columna o diagonal amb dos simbols iguals i un buit. Es
		// juga alli per a guanyar o bloquejar el contrari
		// 2. Jugar en centre
		// 3. Jugar en cantonada
		// 4. Jugar en lateral

		int[][] caselles = tauler.getMatriu();

		// Per cada fila, mirem les combinacions de BUIT, FITXA i FITXA
		for (int fila = 0; fila < caselles.length; fila++) {
			for (int col = 0; col < caselles[0].length; col++) {
				// les iteracions del bucle intern haurien de donar els valors de columna
				// seguents
				// 0 1 2 per a mirar si el 0 i 1 tenen el mateix simbol i el 2 es buit
				// 1 2 0 per a mirar si el 1 i 2 tenen el mateix simbol i el 0 es buit
				// 2 0 1 per a mirar si el 2 i 0 tenen el mateix simbol i el 1 es buit
				// Amb el modul %3 es pot fer
				int primera = col % 3;
				int segona = (col + 1) % 3;
				int tercera = (col + 2) % 3;

				if (caselles[fila][primera] == caselles[fila][segona] && caselles[fila][segona] != Tauler.BUIT
						&& caselles[fila][tercera] == Tauler.BUIT) {
					return new Jugada(fila, tercera);
				}
			}
		}

		// Idem per a columnes
		for (int column = 0; column < caselles.length; column++) {
			for (int fila = 0; fila < caselles[0].length; fila++) {

				int primera = fila % 3;
				int segona = (fila + 1) % 3;
				int tercera = (fila + 2) % 3;

				if (caselles[primera][column] == caselles[segona][column] && caselles[segona][column] != Tauler.BUIT
						&& caselles[tercera][column] == Tauler.BUIT) {
					return new Jugada(tercera, column);
				}
			}
		}

		// Centre
		if (isBuit(caselles[1][1])) {
			return new Jugada(1, 1);
		}

		// Cantonades
		if (caselles[0][0] == Tauler.BUIT) {
			return new Jugada(0, 0);
		}
		if (isBuit(caselles[0][2])) {
			return new Jugada(0, 2);
		}
		if (caselles[2][0] == Tauler.BUIT) {
			return new Jugada(2, 0);
		}
		if (isBuit(caselles[2][2])) {
			return new Jugada(2, 2);
		}

		// Laterals
		if (isBuit(caselles[2][1])) {
			return new Jugada(0, 1);
		}
		if (isBuit(caselles[1][0])) {
			return new Jugada(1, 0);
		}
		if (isBuit(caselles[1][2])) {
			return new Jugada(1, 2);
		}
		if (isBuit(caselles[2][1])) {
			return new Jugada(2, 1);
		}

		return null;
	}

	private boolean isBuit(int casella)
	{
		return (casella == Tauler.BUIT);
	}
	
	// Aquest toString sempre retorna el nom de la classe
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
