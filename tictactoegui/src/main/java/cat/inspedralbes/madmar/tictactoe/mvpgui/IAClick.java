package cat.inspedralbes.madmar.tictactoe.mvpgui;

public class IAClick implements Runnable{

	private IBoardView boardView;
	
	public IAClick(IBoardView boardView) {
		this.boardView=boardView;
	}
	
	@Override
	public void run() {
		boardView.clickTrickButton();		
	}

}
