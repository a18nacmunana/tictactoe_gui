package cat.inspedralbes.madmar.tictactoe.model;

public class Huma extends Jugador {
	
	public Huma(String name,char symbol) {
		super(name,symbol);
	}
	
	@Override
	protected Jugada obtenirJugadaGui(String posicio) {
		
		String[] parts=posicio.split(",");
		
		int fila = Integer.parseInt(parts[0]);
		int col= Integer.parseInt(parts[1]);
		
		return new Jugada(fila, col);
	}
	
	//Aquest toString sempre retorna el nom de la classe
	@Override
	public String toString() {
			return getClass().getSimpleName();
	}

}
