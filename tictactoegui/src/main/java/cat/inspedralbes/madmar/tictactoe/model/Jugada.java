package cat.inspedralbes.madmar.tictactoe.model;

public class Jugada {
	
	private int fila;
	private int col;
	
	public Jugada(int fila, int col) {
		this.setFila(fila);
		this.setCol(col);
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

}
