package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.FlowLayout;

public class PlayerInfoPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelName;
	private JLabel lblSymbol;
	private JLabel lblYourTurn;

	/**
	 * Create the panel.
	 */
	public PlayerInfoPanel() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(10);
		flowLayout.setHgap(10);
		flowLayout.setAlignment(FlowLayout.LEFT);
		
		setPreferredSize(new Dimension(150,200));
		
		JLabel lblPlayerText = new JLabel("Player:");
		lblPlayerText.setPreferredSize(new Dimension(60,30));
		add(lblPlayerText);
		
		labelName = new JLabel("1");
		labelName.setPreferredSize(new Dimension(70,30));
		add(labelName);
		
		JLabel lblSymbolText = new JLabel("Symbol:");
		lblSymbolText.setPreferredSize(new Dimension(60,30));
		add(lblSymbolText);
		
		lblSymbol = new JLabel("X");
		add(lblSymbol);
		
		lblYourTurn = new JLabel("Your turn");
		add(lblYourTurn);
		

	}

	public String getPlayerName() {
		return labelName.getText();
	}
	public void setPlayerName(String text) {
		labelName.setText(text);
	}
	public String getPlayerSymbol() {
		return lblSymbol.getText();
	}
	public void setPlayerSymbol(String text) {
		lblSymbol.setText(text);
	}
	public boolean isLblYourTurnVisible() {
		return lblYourTurn.isVisible();
	}
	public void setLblYourTurnVisible(boolean visible) {
		lblYourTurn.setVisible(visible);
	}
}
