package cat.inspedralbes.madmar.tictactoe.mvpgui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JColorChooser;

import cat.inspedralbes.madmar.tictactoe.model.Huma;
import cat.inspedralbes.madmar.tictactoe.model.IABona;
import cat.inspedralbes.madmar.tictactoe.model.Joc;
import cat.inspedralbes.madmar.tictactoe.model.Jugada;
import cat.inspedralbes.madmar.tictactoe.model.Jugador;
import cat.inspedralbes.madmar.tictactoe.model.Tauler;
import cat.inspedralbes.munnac.a3.JDialog;
import cat.inspedralbes.munnac.a3.MunNacA3AboutDialog;
//import cat.inspedralbes.munnac.a3.JDialog;

//MVP: Definció del presentador.
//Ha d'haver un mètode per cada acció que l'usuari pot fer amb la vista.
public class MyPresenter {
	
	// Vista
	private IFrameView frameView;	
	private IBoardView boardView;
		
	// Model
	private Joc joc;
	private Jugador[] jugadors=new Jugador[2];
		
	public void init(IFrameView view)
	{
		frameView=view;
		boardView=frameView.getBoard();
		String[] defaultSymbols={"X","0"};		
		newGameModel(defaultSymbols);
	}

	void onClickEditSymbols(String player1symbol,String player2symbol) {
		
		if (player1symbol.length()==1&&player2symbol.length()==1&&!player1symbol.equals(player2symbol))
		{			
			char s1=player1symbol.charAt(0);
			char s2=player2symbol.charAt(0);
			
			// Falta fer comprovació dels valors.
			
			// Processat i modificació del model.
			jugadors[0].setSymbol(s1);
			jugadors[1].setSymbol(s2);	
			
			// Actualització de la GUI
			frameView.setPlayersSymbols(s1, s2);
		}
	}	
	
	public void onClickEditColors(Color player1color, Color player2color) {
		jugadors[0].setColor(player1color);
		jugadors[1].setColor(player2color);
		
	}
	
	void onClickBoardButton(ActionEvent arg0) {		
		
		// Processat i modificació del model.
		char symbol = joc.obtenirSymbolJugadorActual();
		Jugada jugada=joc.ferJugadaGui(arg0.getActionCommand());			// Canvia el torn -> Torn futur
		
		// Actualització de la GUI		
		frameView.enableOptionChanges(false);		
		// Mostrar jugada
		boardView.setPlay(jugada,symbol);
		// Mostrar torn
		frameView.showTurn(joc.isTornJugador1());			

		// Mostrar guanyador
		if(!joc.partidaEnJoc())
		{				
			boardView.enableBoard(false);				
			frameView.showWinner("Winner: "+joc.obtenirSymbolGuanyador());			
			frameView.hideTurn();
			frameView.enableOptionChanges(true);
			
		}
		else
		{
			// Si es juga contra la IA
			if (frameView.isIAModeSelected()&&!joc.isTornJugador1()) {
				new Thread(new IAClick(boardView)).start();
			}	
		}
			
	}

	void onClickNewGame()
	{			
		// Processat i modificació del model.	
		newGameModel(frameView.getPlayersSymbol());
		
		// Actualització de la GUI
		frameView.clearGUI();
				
		frameView.showTurn(joc.getTornJugador()==0);
		
	}

	private void newGameModel(String[] symbols) {
				
		Tauler tauler=new Tauler();
		
		jugadors[0]=new Huma("1",symbols[0].charAt(0));
		
		if (frameView.isIAModeSelected()) {
			jugadors[1]=new IABona("2",symbols[1].charAt(0),tauler);
		}		
		else
		{
			jugadors[1]=new Huma("2",symbols[1].charAt(0));
		}
	
		joc=new Joc(tauler, jugadors);
	}
	
	void onClickAbout() {
		MunNacA3AboutDialog dialog = new MunNacA3AboutDialog();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}
	
	void onClickOnline() {
		System.out.println("Estoy en el link");
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
		    try {
				Desktop.getDesktop().browse(new URI("http://www.institutpedralbes.cat"));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}

	}
	
	
}
