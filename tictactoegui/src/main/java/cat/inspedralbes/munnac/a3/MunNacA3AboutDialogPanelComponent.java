package cat.inspedralbes.munnac.a3;

import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.Icon;

public class MunNacA3AboutDialogPanelComponent extends JPanel implements ActionListener{
	
	private JPanel cardPanel;
	private JLabel lblMunnaceditorV;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblImatge;
	private JEditorPane creditEditorPanel;
	private JTextPane llicenciaEditorPane;
	/**
	 * Create the panel.
	 */
	public MunNacA3AboutDialogPanelComponent() {
		//setMaximumSize(new Dimension(450, 470));
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel contenidorPanel = new JPanel();
		contenidorPanel.setPreferredSize(new Dimension(10, 100));
		contenidorPanel.setMinimumSize(new Dimension(10, 100));
		add(contenidorPanel);
		ImageIcon imatge =  scaleImage(new ImageIcon(MunNacA3AboutDialogPanelComponent.class.getResource("/cat/inspedralbes/munnac/a3/img/captura.png")),75,75);
		contenidorPanel.setLayout(new BoxLayout(contenidorPanel, BoxLayout.Y_AXIS));
		
		lblImatge = new JLabel("");
		contenidorPanel.add(lblImatge);
		lblImatge.setIcon(imatge);
		
		lblImatge.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		lblMunnaceditorV = new JLabel("MunNacEditor v1.0");
		contenidorPanel.add(lblMunnaceditorV);
		lblMunnaceditorV.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		cardPanel = new JPanel();
		cardPanel.setMaximumSize(new Dimension(450, 300));
		add(cardPanel);
		CardLayout cl_munNacCL = new CardLayout(0, 0);
		cardPanel.setLayout(cl_munNacCL);
		
		JPanel defaultPanel = new JPanel();
		defaultPanel.setPreferredSize(new Dimension(450, 200));
		defaultPanel.setMaximumSize(new Dimension(450, 200));
		cardPanel.add(defaultPanel, "default");
		defaultPanel.setLayout(new BoxLayout(defaultPanel, BoxLayout.Y_AXIS));
		
		lblNewLabel = new JLabel("Editor senzill amb barra de menú i d'eines");
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		defaultPanel.add(lblNewLabel);
		
		Component verticalGlue = Box.createVerticalGlue();
		verticalGlue.setMaximumSize(new Dimension(0, 100));
		defaultPanel.add(verticalGlue);
		
		lblNewLabel_1 = new JLabel("2020 Nacho Munné");
		lblNewLabel_1.setAlignmentX(Component.CENTER_ALIGNMENT);
		defaultPanel.add(lblNewLabel_1);
		
		/*JPanel llicenciaPanel = new JPanel();
		llicenciaPanel.setPreferredSize(new Dimension(450, 200));
		llicenciaPanel.setMaximumSize(new Dimension(450, 200));
		cardPanel.add(llicenciaPanel, "Llicència");*/
		
		llicenciaEditorPane = new JTextPane();
		llicenciaEditorPane.setPreferredSize(new Dimension(450, 200));
		llicenciaEditorPane.setBounds(1, 1, 429, 21);
		llicenciaEditorPane.setEditable(false);
		
		java.net.URL helpURL = MunNacA3AboutDialogPanelComponent.class.getResource( "/cat/inspedralbes/munnac/a3/htm/license.html");
		if (helpURL != null) {
		    try {
		    	llicenciaEditorPane.setPage(helpURL);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
		    System.err.println("Couldn't find file: license.html");
		}
		
		JScrollPane scrollPane = new JScrollPane(llicenciaEditorPane);
		scrollPane.setPreferredSize(new Dimension(450, 200));
		cardPanel.add(scrollPane, "Llicència");
		
		JPanel creditPanel = new JPanel();
		creditPanel.setPreferredSize(new Dimension(450, 200));
		creditPanel.setMaximumSize(new Dimension(450, 200));
		cardPanel.add(creditPanel, "Crèdits");
		
		creditEditorPanel = new JEditorPane();
		creditEditorPanel.setBounds(0, 0, 430, 139);
		creditEditorPanel.setEditable(false);
		java.net.URL helpURL2 = MunNacA3AboutDialogPanelComponent.class.getResource( "/cat/inspedralbes/munnac/a3/htm/credits.html");
		if (helpURL2 != null) {
		    try {
		    	creditEditorPanel.setPage(helpURL2);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL2);
		    }
		} else {
		    System.err.println("Couldn't find file: credits.html");
		}
		creditPanel.setLayout(null);
		creditPanel.add(creditEditorPanel);
		
		JPanel btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(450, 35));
		btnPanel.setMinimumSize(new Dimension(450, 50));
		btnPanel.setMaximumSize(new Dimension(450, 35));
		btnPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		add(btnPanel);
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		
		JToggleButton llicenciaBtn = new JToggleButton("Llicència");
		llicenciaBtn.addActionListener(this);
		btnPanel.add(llicenciaBtn);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setMinimumSize(new Dimension(20, 30));
		horizontalStrut.setMaximumSize(new Dimension(20, 30));
		horizontalStrut.setPreferredSize(new Dimension(20, 30));
		btnPanel.add(horizontalStrut);
		
		JToggleButton creditBtn = new JToggleButton("Crèdits");
		creditBtn.addActionListener(this);
		btnPanel.add(creditBtn);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setPreferredSize(new Dimension(100, 0));
		horizontalGlue.setMinimumSize(new Dimension(100, 0));
		horizontalGlue.setMaximumSize(new Dimension(100, 0));
		btnPanel.add(horizontalGlue);
		
		JToggleButton tancarBtn = new JToggleButton("Tancar");
		btnPanel.add(tancarBtn);
	}
	
	private ImageIcon scaleImage(ImageIcon imageIcon, int width, int height)
    {       
        Image image = imageIcon.getImage();                                             // transform it
        Image newimg = image.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);     // scale it the smooth way 
        return new ImageIcon(newimg);
    }
	
	public void actionPerformed(ActionEvent arg0) {
		CardLayout cl=(CardLayout) cardPanel.getLayout();
		cl.show(cardPanel,arg0.getActionCommand());
	}
	
	//Program Name
	public String getProgramName() {
		return lblMunnaceditorV.getText();
	}
	public void setProgramName(String text) {
		lblMunnaceditorV.setText(text);
	}
	
	//Program Description
	public String getProgramDescription() {
		return lblNewLabel.getText();
	}
	public void setProgramDescription(String text_1) {
		lblNewLabel.setText(text_1);
	}
	
	//Program copyright
	public String getProgramCopyright() {
		return lblNewLabel_1.getText();
	}
	public void setProgramCopyright(String text_2) {
		lblNewLabel_1.setText(text_2);
	}
	
	//Icon
	public Icon getProgramIcon() {
		return lblImatge.getIcon();
	}
	public void setProgramIcon(Icon icon) {
		lblImatge.setIcon(icon);
	}
	
	//Program credits
	public void setProgramCredits(String url) {
		java.net.URL helpURL = MunNacA3AboutDialogPanelComponent.class.getResource(url);
		if (helpURL != null) {
		    try {
		    	creditEditorPanel.setPage(helpURL);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
		    System.err.println("Couldn't find file: license.html");
		}
	}
	public String getProgramCredits() {
		return creditEditorPanel.getPage().getPath();
	}
	
	//Program license
	public void setProgramLicense(String url) {
		java.net.URL helpURL = MunNacA3AboutDialogPanelComponent.class.getResource(url);
		if (helpURL != null) {
		    try {
		    	llicenciaEditorPane.setPage(helpURL);
		    } catch (IOException e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
		    System.err.println("Couldn't find file: license.html");
		}
	}
	public String getProgramLicense() {
		return llicenciaEditorPane.getPage().getPath();
	}
		
}
