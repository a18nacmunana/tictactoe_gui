package cat.inspedralbes.munnac.a3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MunNacA3AboutDialog extends javax.swing.JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public MunNacA3AboutDialog() {
		//setBounds(100, 100, 450, 300);
		MunNacA3AboutDialogPanelComponent dialogContent = new MunNacA3AboutDialogPanelComponent();
		dialogContent.setPreferredSize(new Dimension(450, 300));
		
		dialogContent.setProgramName("Program name v1.0");
		dialogContent.setProgramCopyright("Copyright <year> <author>");
		dialogContent.setProgramDescription("Program description");
		dialogContent.setProgramCredits("/cat/inspedralbes/munnac/a3/htm/credits.html");
		ImageIcon icon =  scaleImage(new ImageIcon(MunNacA3AboutDialogPanelComponent.class.getResource("/cat/inspedralbes/munnac/a3/img/logo.png")),75,75);
		dialogContent.setProgramIcon(icon);
		dialogContent.setProgramLicense("/cat/inspedralbes/munnac/a3/htm/license.html");
		setContentPane(dialogContent);
		setTitle("About MunNac Tic Tac Toe");
		pack();
	}
	
	private ImageIcon scaleImage(ImageIcon imageIcon, int width, int height)
    {       
        Image image = imageIcon.getImage();                                             // transform it
        Image newimg = image.getScaledInstance(width, height,  java.awt.Image.SCALE_SMOOTH);     // scale it the smooth way 
        return new ImageIcon(newimg);
    }

}
